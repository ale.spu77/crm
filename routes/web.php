<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|test

*/

Auth::routes();
Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
Route::get('/', function(){
	return view('welcome');
});
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/calendario', 'CalendarioController@index')->name('calendario.index');
Route::get('/user', 'ClienteController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
//Route::post('/logout', 'LoginController@logout')->name('logout');
Route::post('/menu', 'HomeController@menu')->name('menu_url');
Route::get('/socket', 'CalendarioController@index')->name('calendario.index');

//Route::post('/menu', 'HomeController@menu')->name('menu_url');
