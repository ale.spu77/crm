<?php

Route::get('/agenda/All', 'AgendaController@agenda')->name('agenda.get');
Route::post('/agenda/horario/', 'AgendaController@horario')->name('agenda.Horario');
Route::post('/agenda/horario/save', 'AgendaController@save')->name('agenda.save');
Route::get('tabla/horarios/{start}/{end}', 'AgendaController@tablaHorario')->name('tablaHorarios.get');