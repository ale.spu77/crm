<?php


Route::post('/empresa/activar/{id}', 'EmpresaController@activar')->name('empresa.activar');
Route::post('/empresa/delete/{id}', 'EmpresaController@delete')->name('empresa.delete');
Route::post('/empresa/{id}', 'EmpresaController@empresa')->name('empresa.get');
Route::get('/empresa/lista', 'EmpresaController@lista')->name('empresa.lista');
Route::post('/empresa/perfil/save', 'EmpresaController@save')->name('empresa.save');