<?php

Route::post('/sucursal/{id}', 'SucursalController@sucursal')->name('sucursal.get');
Route::post('/sucursal/activar/{id}', 'SucursalController@activar')->name('sucursal.activar');
Route::post('/sucursal/delete/{id}', 'SucursalController@delete')->name('sucursal.delete');
Route::post('/sucursal/perfil/save', 'SucursalController@save')->name('sucursal.save');
Route::post('/usuarioSucursal/perfil/save', 'SucursalController@saveUsuarioSucursal')->name('Usuario_sucursal.save');
Route::post('/usuario_sucursal/{id}', 'SucursalController@usuario_sucursal')->name('usuario_sucursal.get');