<?php 


Route::post('/usuario/activar/{id}', 'ClienteController@activar')->name('usuario.activar');
Route::post('/usuario/delete/{id}', 'ClienteController@delete')->name('usuario.delete');
Route::post('/usuario/{id}', 'ClienteController@usuario')->name('usuario.get');
Route::get('/usuario/perfil', 'ClienteController@perfil')->name('usuario.perfil');
Route::get('/usuario/lista', 'ClienteController@lista')->name('usuario.lista');
Route::get('/usuario/calendario', 'ClienteController@calendario')->name('usuario.calendario');

Route::post('/usuario/perfil/save', 'ClienteController@savePerfil')->name('perfil.save');

