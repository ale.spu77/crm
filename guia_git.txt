## configuarar nombre
git config --global user.name "fibaceta"
## ver dato
git config --global user.name

## iniciamos el proyecto
git init

##nos muestra el status de los cambios
git status

## agrega archivos para crear un commit
git add "nombre archivo"
## agrega todos los archivos 
git add -A

## guardamos los cambios con commit
git commit -m "Mensaje"

## nos muestra los commit anteriores
git log

## con este comando podemos viajar en distintos commit
git checkout "codigo sha"

## revisamos las ramas del proyecto
git branch

## nos cambiamos de ramas
git checkout "nombre rama"

## creamos una nueva rama
git branch "nombre rama"

## para borrar las ramas 
git branch -d "nombre de la rama"

## hacemos una fusion de rama
## entramos a master git checkout master
git merge "nombre de la rama"














