<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
	
    'facebook' => [
        'client_id' => '1079368022410252',
        'client_secret' => '2f6be455d67290322bc84671b8d9728a',
        'redirect' => 'http://localhost:8000/auth/facebook/callback'
    ],

    'google' => [
        'client_id' => '269400500845-58nu99kbuc2vd71bue0m1ihvl7btq1pf.apps.googleusercontent.com',
        'client_secret' => 'ZB8Gv37ywIeu6YeXda--ykai',
        'redirect' => 'http://localhost:8000/auth/google/callback',

        /*"client_id":"541407580812-6l73jvetq7kved83tbsn5is626poa4gt.apps.googleusercontent.com",
        "project_id":"neirbour-1585382332098",
        "auth_uri":"https://accounts.google.com/o/oauth2/auth",
        "token_uri":"https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs",
        "client_secret":"MP04u4vrwl1Es-dLtcJ7Gi7B",
        "javascript_origins":["http://localhost:8000"]*/
    ],

];
