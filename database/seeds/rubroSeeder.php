<?php

use Illuminate\Database\Seeder;
use App\rubro;

class rubroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $rubros=[
        ['nombre' => "Agricultura, Ganadería, Silvicultura y Pesca",],
        ['nombre' => "Explotación de Minas y Canteras",],
        ['nombre' => "Industrias Manufacturera",],
        ['nombre' => "Suministro de Electricidad, Gas, Vapor y Aire Acondicionado",],
        ['nombre' => "Suministro de Agua; Evacuación de Agua residuales, gestión de desechos y descontaminación",],
        ['nombre' => "Construcción",],
        ['nombre' => "Comercio al Por Mayor y al por Menor; Reparación de Vehículos Automotores y Motocicletas",],
        ['nombre' => "Transporte y Almacenamiento",],
        ['nombre' => "Actividades de Alojamiento y de Servicio de Comidas",],
        ['nombre' => "Información y Comunicaciones",],
        ['nombre' => "Actividades Financieras y de Seguros",],
        ['nombre' => "Actividades inmobiliarias",],
        ['nombre' => "Actividades Profesionales, Cientificas y Técnicas",],
        ['nombre' => "Actividades de Servicios Administrativos y de Apoyo",],
        ['nombre' => "Adm. Pública y Defensa; Planes de Seguridad Social de Afiliación Obligatoria",],
        ['nombre' => "Enseñanza",],
        ['nombre' => "Actividades de Atención de la Salud Humana y de Asistencia Social",],
        ['nombre' => "Actividades Artísticas, de Entretenimiento y Recreativas",],
        ['nombre' => "Otras Actividades de Servicios",],
        ['nombre' => "Actividades de los Hogaes como Empleadores; Actividades No Diferenciadas de los Hogares",],
        ['nombre' => "Actividades de Organizaciones y Órganos Extraterritoriales",],
    ];

    foreach ($rubros as $Item){
        DB::table('rubro')->insert(['nombre' => $Item["nombre"],]);
        
    }

        
    }
}
