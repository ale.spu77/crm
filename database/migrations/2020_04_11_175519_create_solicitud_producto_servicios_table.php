<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudProductoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_producto_servicio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_solicitud')->unsigned();
            $table->bigInteger('id_producto_servicio')->unsigned();
            $table->timestamps();
            $table->foreign('id_solicitud')->references('id')->on('solicitud')->constrained();
            $table->foreign('id_producto_servicio')->references('id')->on('producto_servicio')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_producto_servicio');
    }
}
