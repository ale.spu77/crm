<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloqueoHorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloqueo_horas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_agenda')->unsigned();
            $table->bigInteger('id_solicitud');
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->timestamps();
            $table->foreign('id_agenda')->references('id')->on('agenda')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloqueo_horas');
    }
}
