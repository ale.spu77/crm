<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaProductoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_producto_servicio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_agenda')->unsigned();
            $table->bigInteger('id_producto_servicio')->unsigned();
            $table->bigInteger('cantidad');
            $table->timestamps();
            $table->foreign('id_agenda')->references('id')->on('agenda')->constrained();
            $table->foreign('id_producto_servicio')->references('id')->on('producto_servicio')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_producto_servicio');
    }
}
