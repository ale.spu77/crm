<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_agenda')->unsigned();
            $table->bigInteger('id_usuario')->unsigned();
            $table->bigInteger('id_estado')->unsigned();
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->timestamps();
            $table->foreign('id_agenda')->references('id')->on('agenda')->constrained();
            $table->foreign('id_usuario')->references('id')->on('users')->constrained();
            $table->foreign('id_estado')->references('id')->on('estado_solicitud')->constrained();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud');
    }
}
