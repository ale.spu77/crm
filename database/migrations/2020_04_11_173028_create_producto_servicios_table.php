<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_servicio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tipo')->unsigned();
            $table->char('nombre', 50);
            $table->char('descripcion', 250);
            $table->bigInteger('valor')->unsigned();
            $table->bigInteger('duracion')->unsigned();
            $table->char('imagen', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_servicio');
    }
}
