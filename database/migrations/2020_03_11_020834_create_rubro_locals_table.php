<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRubrolocalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rubro_empresa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rubro_id')->unsigned(); 
            $table->bigInteger('empresa_id')->unsigned(); 
            $table->foreign('rubro_id')->references('id')->on('rubro')->constrained();
            $table->foreign('empresa_id')->references('id')->on('empresa')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rubro_empresa');
    }
}
