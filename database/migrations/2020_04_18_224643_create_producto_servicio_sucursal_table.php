<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoServicioSucursalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_servicio_sucursal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('producto_servicio_id')->unsigned(); 
            $table->bigInteger('sucursal_id')->unsigned(); 
            $table->foreign('producto_servicio_id')->references('id')->on('producto_servicio')->constrained();
            $table->foreign('sucursal_id')->references('id')->on('sucursal')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_servicio_sucursal', function (Blueprint $table) {
            //
        });
    }
}
