<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agenda_producto_servicio extends Model
{
    protected $table = 'agenda_producto_servicio';
    protected $primaryKey = 'id';
}
