<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class empresa extends Model
{
    protected $table = 'empresa';
    protected $primaryKey = 'id';
    //public $timestamps = false;

    protected $fillable = ['id', 'nombre', 'activado'];

    public function rubro_empresa(){
    return $this->hasMany(rubro_empresa::class);
    //return $this->belongsToMany(rubro_local::class)->withTimestamps();
  }

  public function sucursal(){
    return $this->hasMany(sucursal::class, 'id_empresa', 'id');
    //return $this->belongsToMany(rubro_local::class)->withTimestamps();
  }
}
