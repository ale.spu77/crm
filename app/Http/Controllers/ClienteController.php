<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function activar($id,Request $request){
         $usuario = User::find($id);
         $usuario->activado = true;
         $usuario->save();
        return response()->json($usuario);

    }
    public function delete($id,Request $request){
         $usuario = User::find($id);
         $usuario->activado = false;
         $usuario->save();
        return response()->json($usuario);

    }
    public function usuario($id){

        $usuario = User::with('roles')->find($id);
        $usuario->roles_user = $usuario->roles->pluck('id');
        //dd($usuario->roles->pluck('id'));
        //dd($usuario);
        return response()->json($usuario);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login_cliente');
    }

    public function perfil(){

        return view('usuario.perfilUsuario');
    }
    public function lista(){

        $usuarios = User::all();

        return view('usuario.lista')->with(compact('usuarios'));
    }

    public function calendario(){

        return view('usuario.calendario');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function savePerfil(Request $request)
    {   
        if ($request->id) {
            $user = User::find($request->id);
        }else{
            $user = new User;
        }
        
        $user->name  = $request->nombre;
        $user->email = $request->email;
        $user->direccion = $request->direccion;
        $user->telefono = $request->telefono;
        $user->activado = true;
        $user->mensaje   = $request->mensaje;
        if ($request->clave) {
            $user->password = bcrypt($request->clave);
        }
        $user->save();
        if ($request->roles) {
            $user->syncRoles($request->roles);
        }
        return response()->json($user);
        dd($request->all());
    }

}
