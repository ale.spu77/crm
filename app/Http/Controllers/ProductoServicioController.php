<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\producto_servicio;

class ProductoServicioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    

    public function save(Request $request){

        //dd($request);

        if ($request->id) {
            $ps = producto_servicio::find($request->id);
        }else{
            $ps = new producto_servicio;
        }
        
        $ps->nombre  = $request->nombre;
        $ps->tipo  = $request->tipo;  
        $ps->descripcion  = $request->descripcion;  
        $ps->valor  = $request->valor;  
        $ps->duracion  = $request->duracion;
        $ps->imagen  = base64_encode($request->imagen);     
        $ps->save();

        
        

        
        return response()->json($ps);
        dd($request->all());
    }
}
