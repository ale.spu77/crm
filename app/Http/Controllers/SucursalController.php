<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\sucursal;
use App\local;
use App\usuario_sucursal;

class SucursalController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function activar($id, Request $request){
        
        $sucursal = sucursal::find($id);
        $sucursal->activado = true;
        $sucursal->save();
        return response()->json($sucursal);
        

    }
    public function delete($id, Request $request){
        
        $sucursal = sucursal::find($id);
        $sucursal->activado = false;
        $sucursal->save();
        return response()->json($sucursal);
        
    }

    public function save(Request $request){

        //dd($request);

        if ($request->id) {
            $sucursal = sucursal::find($request->id);
        }else{
            $sucursal = new sucursal;
            $sucursal->activado  = false;
        }
        
        $sucursal->nombre  = $request->nombre;
        $sucursal->id_empresa = $request->empresa;
        $sucursal->direccion = $request->direccion;       
        $sucursal->save();
        
        return response()->json($sucursal);
        dd($request->all());
    }

    public function sucursal($id){

        $sucursal = sucursal::with('empresa')->find($id);
        $sucursal->empresas = $sucursal->empresa->pluck('id');

        //dd($sucursal);
    
        return response()->json($sucursal);
    }

    public function usuario_sucursal($id){

        $sucursal = sucursal::with('usuario_sucursal')->find($id);
        $sucursal->usuario_sucursals = $sucursal->usuario_sucursal->pluck('id_usuario');

        //dd($sucursal);
    
        return response()->json($sucursal);
    }

    public function saveUsuarioSucursal(Request $request){

        dd($request);

        if ($request->rubro) {
            foreach($request->rubro as $ru){
                $rubro_loc=rubro_local::where('rubro_id', $ru)
                                        ->where('local_id', $request->id)
                                        ->First();

                if(!$rubro_loc){
                    $rubro_loc = new rubro_local;
                    $rubro_loc->rubro_id=$ru;
                    $rubro_loc->local_id=$request->id;
                    $rubro_loc->save();
                }
            }
        }

        if ($request->id) {
            $sucursal = sucursal::find($request->id);
        }else{
            $sucursal = new sucursal;
            $sucursal->activado  = false;
        }
        
        $sucursal->nombre  = $request->nombre;
        $sucursal->id_local = $request->local;
        $sucursal->direccion = $request->direccion;
        $sucursal->id_empresa = 0;        
        $sucursal->save();
        
        return response()->json($sucursal);
        dd($request->all());
    }
}
