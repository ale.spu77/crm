<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\rubro;
use App\empresa;
use jeremykenedy\LaravelRoles\Models\Role;
use App\sucursal;
use App\agenda;
use App\producto_servicio;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $view = view('home')->render(); 
        return $view;
    }

    public function menu(Request $request){

        $item = $request->item;

        switch ($item) {
            
            case 'usuario.lista':
                $usuarios = User::all();
                $roles = Role::all();

                return view($item,compact('usuarios','roles'));
            break;
            case 'sucursal.lista':
                $empresas = empresa::all();
                $sucursales = Sucursal::all();
                $usuarios = User::all();
                return view($item,compact('sucursales','empresas', 'usuarios'));
            break;
            case 'sucursal.lista.empresa':
                //dd($request->id);
                $id=$request->id;
                $empresas = empresa::all();              
                $sucursales = Sucursal::where('id_empresa', $id)->get();
                $usuarios = User::all();
                //dd($sucursales);
                return view('sucursal.lista',compact('sucursales','empresas', 'usuarios'));
            break;
            case 'usuario.perfilUsuario':
                $roles = Role::all();
                return view($item,compact('roles'));
            break;
            case 'local.nuevo':
                $rubros = rubro::all();
                return view($item,compact('rubros'));
            break;

            case 'empresa.lista':
                $empresas = empresa::all();
                $rubros = rubro::all();
                $sucursales = sucursal::all();
                return view($item, compact('empresas', 'rubros', 'sucursales'));
            break;
            case 'productoServicio.lista':
                $producto_servicio = producto_servicio::all();
                $empresas = empresa::all();
                $rubros = rubro::all();
                $sucursales = sucursal::all();
                return view($item, compact('empresas', 'rubros', 'sucursales', 'producto_servicio'));
            break;
            case 'producto.index':
			$dataTable = new \App\DataTables\ProductosDataTable;
                return $dataTable->render($item);
                return view($item, compact('dataTable'));
            break;

            case 'agenda.lista':
                $agenda = agenda::all();
                $producto = producto_servicio::where('tipo', 2)->get();
                $servicio = producto_servicio::where('tipo', 1)->get();
                return view($item, compact('agenda', 'producto', 'servicio'));
            break;

            default:
                return view($item);
            break;
        }

    }

    public function socket(){
        dd(22);
    }


}
