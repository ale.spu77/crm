<?php

namespace App\Http\Controllers;

use App\empresa;
use App\rubro;
use App\rubro_empresa;
use Illuminate\Http\Request;


class EmpresaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function activar($id, Request $request){

        $empresas = empresa::find($id);
        $empresas->activado = true;
        $empresas->save();
       return response()->json($empresas);

   }
   public function delete($id, Request $request){

        $empresas = empresa::find($id);
        $empresas->activado = false;
        $empresas->save();
       return response()->json($empresas);

   }

    public function empresa($id){

        $empresas = empresa::with('rubro_empresa')->with('sucursal')->find($id);

        $empresas->rubro_empresas = $empresas->rubro_empresa->pluck('rubro_id');
        $empresas->sucursals = $empresas->sucursal->pluck('id_empresa');
    
        return response()->json($empresas);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function save(Request $request){

        //dd($request);

        if ($request->id) {
            $empresas = empresa::find($request->id);
        }else{
            $empresas = new empresa;
            $empresas->activado  = false;
        }
        
        $empresas->nombre  = $request->nombre;        
        $empresas->save();

        if ($request->rubro) {
            foreach($request->rubro as $ru){
                $rubro_emp=rubro_empresa::where('rubro_id', $ru)
                                        ->where('empresa_id', $request->id)
                                        ->First();

                if(!$rubro_emp){
                    $rubro_emp = new rubro_empresa;
                    $rubro_emp->rubro_id=$ru;
                    $rubro_emp->empresa_id=$request->id;
                    $rubro_emp->save();
                }
            }
        }
        

        
        return response()->json($empresas);
        dd($request->all());
    }

}
