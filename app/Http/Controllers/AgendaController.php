<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\agenda;
use App\agenda_producto_servicio;
use Auth;

class AgendaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function agenda(Request $request){

        $agenda = agenda::whereBetween('fecha', [$request->start, $request->end])->get();

        //$event = agenda::whereBetween('fecha', [$request->start, $request->end])->get();
        
        //dd($event);

        foreach($agenda as $row){
            $data[] = array(
            'id'   => 'no',
            'start'   => $row["fecha"]."T".$row["hora_inicio"],
            'end'   => $row["fecha"]."T".$row["hora_fin"],
            'rendering' => 'inverse-background'
            );
        }

        //array_push($array, $x);

        echo json_encode($data);
    }

    public function horario(Request $request){
        $agenda = agenda::with('agenda_producto_servicio')->find($request->id); 

        $agenda->agenda_producto_servicios = $agenda->agenda_producto_servicio->pluck('id_producto_servicio');
        
        //dd($agenda);
        return response()->json($agenda);        
    }

    public function tablaHorario(Request $request){

        //dd($request);

        $agenda = agenda::whereBetween('fecha', [$request->start, $request->end])->get(); 

        
        
        return view('agenda.horario',compact('agenda'));

        
    }

    public function save(Request $request){

        //dd($request);

            if ($request->id) {
                $agenda = agenda::find($request->id);
                
            }else{
                    $agenda = new agenda;
                    $agenda->id_usuario  = Auth::user()->id;
                    $agenda->fecha  = $request->fecha;
                    $agenda->hora_inicio  = $request->hora_inicio;   
                    $agenda->hora_fin  = $request->hora_fin;      
                    $agenda->save();

            }

            $agenda->fecha  = $request->fecha;
            $agenda->hora_inicio  = $request->hora_inicio;   
            $agenda->hora_fin  = $request->hora_fin;      
            $agenda->save();

            

            if ($request->estado) {
                foreach($request->estado as $estado){
                    //echo $estado;
                    $agendaPS=agenda_producto_servicio::where('id_producto_servicio', $estado)
                                        ->where('id_agenda', $agenda->id)
                                        ->First();

                    if(!$agendaPS){
                        $agendaPS = new agenda_producto_servicio;
                        $agendaPS->id_producto_servicio=$estado;
                        $agendaPS->id_agenda=$agenda->id;
                        $agendaPS->cantidad=0;
                        $agendaPS->save();
                    }
                }
            }



        /*
        if ($request->id) {
            $agenda = agenda::find($request->id);
        }else{
            $agenda = new agenda;
            $empresas->activado  = false;
        }

        /*

        if ($request->id) {
            $empresas = empresa::find($request->id);
        }else{
            $empresas = new empresa;
            $empresas->activado  = false;
        }
        
        $empresas->nombre  = $request->nombre;        
        $empresas->save();

        if ($request->rubro) {
            foreach($request->rubro as $ru){
                $rubro_emp=rubro_empresa::where('rubro_id', $ru)
                                        ->where('empresa_id', $request->id)
                                        ->First();

                if(!$rubro_emp){
                    $rubro_emp = new rubro_empresa;
                    $rubro_emp->rubro_id=$ru;
                    $rubro_emp->empresa_id=$request->id;
                    $rubro_emp->save();
                }
            }
        }

        */
    }

}
