<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estado_solicitud extends Model
{
    protected $table = 'estado_solicitud';
    protected $primaryKey = 'id';
}
