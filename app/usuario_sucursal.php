<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_sucursal extends Model
{
    protected $table = 'usuario_sucursal';
    protected $primaryKey = 'id';
}
