<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    //
 	protected $table = 'sucursal';
    protected $primaryKey = 'id';

    public function empresa()
    {
      //return $this->hasMany(rubro_local::class);
      return $this->belongsTo('App\empresa', 'id_empresa');
    }

    public function usuario_sucursal(){
      return $this->hasMany(usuario_sucursal::class, 'id_sucursal', 'id');
      //return $this->belongsToMany(rubro_local::class)->withTimestamps();
    }

}
