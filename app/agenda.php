<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agenda extends Model
{
    protected $table = 'agenda';
    protected $primaryKey = 'id';

    public function agenda_producto_servicio(){
        return $this->hasMany(agenda_producto_servicio::class, 'id_agenda', 'id');
      }
}
