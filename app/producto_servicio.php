<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto_servicio extends Model
{

 	protected $table = 'producto_servicio';
    protected $primaryKey = 'id_producto_servicio';
    //public $timestamps = false;

    protected $fillable = ['id', 'tipo', 'nombre', 'descripcion', 'valor', 'duracion', 'imagen'];
}
