<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rubro_empresa extends Model
{
    protected $table = 'rubro_empresa';
    protected $primaryKey = 'id';
    //public $timestamps = false;

    protected $fillable = ['id', 'id_rubro', 'id_empresa'];


    public function empresa()
  {
    //return $this->hasMany(rubro_local::class);
    return $this->belongsToMany(empresa::class)->withTimestamps();
  }
}
