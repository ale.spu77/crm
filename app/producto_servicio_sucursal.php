<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto_servicio_sucursal extends Model
{
    protected $table = 'producto_servicio_sucursal';
    protected $primaryKey = 'id';
}
