<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Lista de Usuario</h4>
            <h6 class="card-subtitle pull-right" style="float: right;">
                <button onclick="Usuario.nuevo()" type="button" class="btn btn-info">Nuevo Usuario</button>
            </h6>
            <div class="table-responsive m-t-40">
                <table id="usuarios" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th>Fecha</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                   
                    <tbody>
                    	@foreach($usuarios as $usuario)
                        <?php $row = $usuario->activado ? 'table-success':'table-danger' ?>
                        <tr class="{{$row}}">
                            <td>{{ $usuario->id }}</td>
                            <td>{{ $usuario->name }}</td>
                            <td>{{ $usuario->email }}</td>
                            <td>{{ $usuario->roles->implode('name',' - ') }}</td>
                            <td>{{ $usuario->created_at }}</td>
                            <td>
                                <button 
                                class="btn btn-primary" 
                                onclick="Usuario.modalUsuario({{$usuario->id}})">Editar</button>
                                @if($usuario->activado)
                                <button 
                                    class="btn btn-danger" 
                                    onclick="Usuario.deleteUsuario({{$usuario->id}})">Desactivar</button>
                                @else
                                <button 
                                    class="btn btn-success" 
                                    onclick="Usuario.activarUsuario({{$usuario->id}})">Activar</button>
                                @endif
                            </td>
                        </tr>
                    	@endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-user">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_usuario">
                    <input type="hidden" name="id">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                             <div class="form-group">
                                <label for="">Nombre</label>
                                <input type="text" class="form-control" name="nombre"  required="required">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" name="email" required="required">
                            </div>
                            <div class="form-group">
                                <label for="">Direccion</label>
                                <input type="text" class="form-control" name="direccion" required="required">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                   
                            <div class="form-group">
                                <label for="">Telefono</label>
                                <input type="text" class="form-control" name="telefono" required="required">
                            </div>
                            <label for="roles">Roles</label>
                            @foreach($roles as $rol)
                                <div class="demo-checkbox">
                                    <input type="checkbox" 
                                        id="roles_{{$rol->id}}" 
                                        name="roles[]" 
                                        value="{{ $rol->id }}" 
                                        class="filled-in chk-col-indigo" >
                                    <label 
                                        for="roles_{{$rol->id}}">
                                        {{$rol->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_usuario" onclick="Usuario.formSave()" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">
	
$(document).ready(function() {

    Usuario = {
        url_usuario:"{{ route('perfil.save') }}",
        //menu:$('a.menu_item.active'),
        token:"{{csrf_token()}}",
        init:function(){
            $('form').on('submit', function (ev) {
              ev.preventDefault();
            });
        },
        activarUsuario:function(id){

            $.ajax({
                url:"{{url('usuario/activar')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
                //tr.removeClass('table-danger');
                //tr.addClass('table-success');
                $('a.menu_item.active').click();
            });

        },
        deleteUsuario:function(id){

            $.ajax({
                url:"{{url('usuario/delete')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
               $('a.menu_item.active').click();
            });
        },
        nuevo:function(e){

            $('#form_usuario input[name="id"]').val('');
            $('#form_usuario input[name="nombre"]').val('');
            $('#form_usuario input[name="email"]').val('');
            $('#form_usuario input[name="direccion"]').val('');
            $('#form_usuario input[name="telefono"]').val('');
            $('.demo-checkbox input').prop('checked',false);
            $('#modal-user').modal('show');

        },
        modalUsuario:function(id){
            $('#form_usuario input[name="id"]').val('');
            $('#form_usuario input[name="nombre"]').val('');
            $('#form_usuario input[name="email"]').val('');
            $('#form_usuario input[name="direccion"]').val('');
            $('#form_usuario input[name="telefono"]').val('');
            $('.demo-checkbox input').prop('checked',false);

            $.ajax({
                url:"{{url('usuario')}}/"+id,
                type:"POST",
                data:{
                    _token:this.token
                }
            }).done(function(data){

                if(data){
                    $('#form_usuario input[name="id"]').val(data.id);
                    $('#form_usuario input[name="nombre"]').val(data.name);
                    $('#form_usuario input[name="email"]').val(data.email);
                    $('#form_usuario input[name="direccion"]').val(data.direccion);
                    $('#form_usuario input[name="telefono"]').val(data.telefono);
                    $.each(data.roles_user,function(i,v){
                        
                        $('.demo-checkbox input[value="'+v+'"]').prop('checked',true);
                    });
                }

            });

            $('#modal-user').modal('show');
        },
        formSave:function(){
            var formElement = document.getElementById("form_usuario");
            var ajax = App.ajaxData(this.url_usuario,formElement);

            if (ajax){
                ajax.done(function(data){
                    //$('a.menu_item.active').click();
                    $('#modal-user').modal('hide');
                });
            }
        }
    }
    
    Usuario.init();
/*
	$('#usuarios').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    */
});

</script>
