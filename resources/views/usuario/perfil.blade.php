@extends('layouts.admin_navbar')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Datos de Usuario</h4>
                <form id="formPerfil">
                    <input type="hidden" value="{{ csrf_token()}}" name="_token">

                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label for="validationDefaultUsername">Nombre de Usuario</label>
                          <div class="input-group">
                            <input name="usuario" type="text" class="form-control" id="validationDefaultUsername"  aria-describedby="input03" required>
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">z
                          <label for="input01">Nombres</label>
                          <input name="nombres" type="text" class="form-control" value="{{ Auth::user()->name }}" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="input02">Apellido</label>
                          <input name="apellido" type="text" class="form-control" value="{{ Auth::user()->name }}" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="input02">ROl</label>
                          <select class="form-control">
                            <option>Admin</option>
                            <option>Usuario</option>
                          </select>
                        </div>
                        <bs
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                          <input name="admin" class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                          <label class="form-check-label" for="invalidCheck2">
                            Administrador
                          </label>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="button" onclick="Perfil.saveForm()">Guardar</button>
                </form>
            </div>
        </div>
       
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  
  var Perfil = {
    form:$('#formPerfil'),
    saveForm:function(){
        formData = new FormData(this.form[0]);
        console.log(this.form[0]);
        console.log(formData);
        this.sendAjax('{{route("perfil.save")}}',formData);
    },
    sendAjax:function(route,formData){
      var ajax = $.ajax({
          url: route,
          data: formData,
          type: 'POST',
          contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
          processData: false, // NEEDED, DON'T OMIT THIS
        });
      return ajax;
    }

  }

</script>
@endsection