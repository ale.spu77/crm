
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Lista de Sucursales</h4>
        <h6 class="card-subtitle pull-right" style="float: right;">
            <button onclick="Sucursal.nuevo()" type="button" class="btn btn-info">Nuevo Sucursal</button>
        </h6>
        <div class="table-responsive m-t-40">
            <table id="sucursal" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Empresa</th>
                        <th>direccion</th>
                        <th>Fecha</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
               
                <tbody>
                	@foreach($sucursales as $sucursal)
                    <?php $row = $sucursal->activado ? 'table-success':'table-danger' ?>
                    <tr class="{{$row}}">
                        <td>{{ $sucursal->id }}</td>
                        <td>{{ $sucursal->nombre }}</td>
                        <td>{{ $sucursal->empresa->nombre }}</td>
                        <td>{{ $sucursal->direccion }}</td>
                        <td>{{ $sucursal->created_at }}</td>
                        <td>
                            <button class="btn btn-primary" onclick="Sucursal.modalSucursal({{$sucursal->id}})">Editar</button>

                            @if($sucursal->activado)
                            <button 
                                class="btn btn-danger" 
                                onclick="Sucursal.deleteSucursal({{$sucursal->id}})">Desactivar</button>
                            @else
                            <button 
                                class="btn btn-success" 
                                onclick="Sucursal.activarSucursal({{$sucursal->id}})">Activar</button>
                            @endif

                            <button class="btn btn-info" onclick="Usuario_sucursal.modalUsuario({{$sucursal->id}})">Usuarios</button>
                        </td>

                        
                    </tr>
                	@endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!---Modal Sucursal--->
<div class="modal fade" id="modal-sucursal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sucursal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_sucursal">
                    <input type="hidden" name="id">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="local"> Selecciona Empresa: <span class="danger">*</span> </label>
                                <select class="form-control" name="empresa" id="empresa" required="required">

                                    <option value="0" selected disabled>Selecciona Empresa</option>
                                    @foreach($empresas as $empresa)  
                                    <option value="{{ $empresa->id }}">{{ $empresa->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre"> Nombre Sucursal: <span class="danger">*</span> </label>
                                <input type="text" class="form-control" name="nombre" required="required"> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="direccion"> Dirección Sucursal: <span class="danger">*</span> </label>
                                <input type="text" class="form-control" name="direccion" required="required"> 
                            </div>                            
                        </div>

                    </div>
                                  

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_sucursal" onclick="Sucursal.formSave()" type="button" class="btn btn-primary" data-dismiss="modal" data-backdrop="false">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!---Modal Sucursal--->
<link href="{{asset('assets/plugins/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />

<!---Modal Usuario--->
<div class="modal fade" id="modal-usuarios">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuarios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_usuarios">
                    <input type="hidden" name="id">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                      
                    <div class="row">
                        
                        <div class="col-md-12">
                            <select id='usuarios_selec' name='usuarios_selec' multiple='multiple'>
                            @foreach($usuarios as $usuario)
                                <option value='{{ $usuario->id }}'>{{ $usuario->name }}</option>
                            @endforeach                  
                            </select>

                            <div class="button-box m-t-20"> 
                                <a id="select-all" class="btn btn-danger" href="#">seleccionar Todos</a> 
                                <a id="deselect-all" class="btn btn-info" href="#">Quitar todos</a> 
                            </div>                                 
                        </div>
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_usaurio" onclick="Usuario_sucursal.formSave()" type="button" class="btn btn-primary" data-dismiss="modal" data-backdrop="false">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!---Modal Usuario--->



<script type="text/javascript" src="{{asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript">
	
$(document).ready(function() {

Sucursal = {
    url_sucursal:"{{ route('sucursal.save') }}",
    //menu:$('a.menu_item.active'),
    token:"{{csrf_token()}}",
    init:function(){
        $('form').on('submit', function (ev) {
          ev.preventDefault();
        });
    },
    activarSucursal:function(id){
        $.ajax({
                url:"{{url('sucursal/activar')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
                $('a.menu_item.active').click();
            });           

    },
    deleteSucursal:function(id){
        $.ajax({
                url:"{{url('sucursal/delete')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
               $('a.menu_item.active').click();
            });
    },
    nuevo:function(e){
        $('#form_sucursal input[name="id"]').val('');
        $('#form_sucursal input[name="nombre"]').val('');
        $('#form_sucursal input[name="direccion"]').val('');
        $('#empresa > option').prop('selected',false);
        $('#modal-sucursal').modal('show');

    },
    modalSucursal:function(id){
        $('#form_sucursal input[name="id"]').val('');
        $('#form_sucursal input[name="nombre"]').val('');
        $('#form_sucursal input[name="direccion"]').val('');
        $('#empresa > option').prop('selected',false);

            $.ajax({
                url:"{{url('sucursal')}}/"+id,
                type:"POST",
                data:{
                    _token:this.token
                }
            }).done(function(data){

                if(data){

                    $('#form_sucursal input[name="id"]').val(data.id);
                    $('#form_sucursal input[name="nombre"]').val(data.nombre);
                    $('#form_sucursal input[name="direccion"]').val(data.direccion);
                    $('#empresa > option[value="'+data.id_empresa+'"]').prop('selected',true);

                }

            });

            $('#modal-sucursal').modal('show');
        }, 
    formSave:function(){
        
        var formElement = document.getElementById("form_sucursal");
        
        var ajax = App.ajaxData(this.url_sucursal,formElement);

        if (ajax){
            ajax.done(function(data){
                $('#modal-sucursal').modal('hide');               
                $('a.menu_item.active').click();
            });
        }
    }
}

Sucursal.init();

Usuario_sucursal = {
    url_usu:"{{ route('Usuario_sucursal.save') }}",
    //menu:$('a.menu_item.active'),
    token:"{{csrf_token()}}",
    init:function(){
        $('form').on('submit', function (ev) {
          ev.preventDefault();
        });
    },
    modalUsuario:function(id){

        $('#form_usuarios input[name="id"]').val('');
        $('#usuarios_selec > option').prop('selected',false);

            $.ajax({
                url:"{{url('usuario_sucursal')}}/"+id,
                type:"POST",
                data:{
                    _token:this.token
                }
            }).done(function(data){

                if(data){

                    $('#form_usuarios input[name="id"]').val(data.id);
                    $.each(data.usuario_sucursals,function(i,v){
                        //alert(v);
                        $('#usuarios_selec > option[value="'+v+'"]').attr('selected', 'selected');
                    });
                    $('#usuarios_selec').multiSelect('refresh');
                }

            });

            

            $('#modal-usuarios').modal('show');
        }, 
    formSave:function(){

        var formElement = document.getElementById("form_usuarios");
        
        var ajax = App.ajaxData(this.url_usu,formElement);

        if (ajax){
            ajax.done(function(data){
                $('#modal-usuarios').modal('hide');               
                $('a.menu_item.active').click();
            });
        }
    }
}

 // For multiselect
$('#usuarios_selec').multiSelect();
$('#select-all').click(function() {
    $('#usuarios_selec').multiSelect('select_all');
    return false;
});
$('#deselect-all').click(function() {
    $('#usuarios_selec').multiSelect('deselect_all');
    return false;
});
              
        

});

</script>
