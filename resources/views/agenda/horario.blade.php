<!--Little Profile widget-->
<div class="col-lg-12">
    <div class="card">
        <div class="card-body little-profile text-center">
            <div class="pro-img m-t-20"><img src="{{ Auth::user()->avatar }}" alt="user"></div>
            <h3 class="m-b-0">{{ auth()->user()->name }}</h3>
            <h6 class="text-muted">Web Designer &amp; Developer</h6>
            <ul class="list-inline soc-pro m-t-30">
                <li><a href="javascript:void(0)"><i class="fab fa-twitter"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fab fa-facebook-square"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fab fa-google-plus-g"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fab fa-youtube"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fab fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="d-flex no-block">
                <h4 class="card-title"><span class="lstick"></span>Bloques de Horario</h4>
                <div class="btn-group ml-auto m-t-10">
                    <a href="JavaScript:void(0)" class="icon-plus link" onclick="Horario.modalNuevo()"></a>                    
                </div>
            </div>
            <div class="message-box contact-box">
                <div class="message-widget contact-widget">
                                        
                    <!-- Bloque de Horario -->
                    @foreach($agenda as $agen)
                    <a href="javascript:void(0)" onclick="Horario.modalHorario({{ $agen->id }})">
                        <div class="user-img"> <span class="round">BH</span> <span class="profile-status away pull-right"></span> </div>
                            <div class="mail-contnet">
                                <h5>ID:{{ $agen->id }} - {{ $agen->fecha }}</h5> <span class="mail-desc">{{ $agen->hora_inicio }} - {{ $agen->hora_fin }}</span>
                            </div>
                    </a>
                    @endforeach
                                        
                </div>
            </div>
        </div>
    </div>
</div>
<!--Little Profile widget-->