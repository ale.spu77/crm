<link href="{{ asset('assets/plugins/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />
<style>
#calendario .fc-bgevent {
    background: #000000;
}
#calendario.agenda{
    background-color: #FF9900;
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="">
                <div class="row">
                    <div class="col-lg-3">
                    <div id="tabla_horarios"></div>
                    </div>
                    <div class="col-lg-9">
                        <div class="card-body b-l calender-sidebar">
                            <div id="calendario"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-horario">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Horario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_horario">
                    
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-sm-12">

                                <div id="horarios_turno"></div>
                                <div class="row">
                                    <div class="col-sm-4 nopadding">
                                        <div class="form-group">
                                            <input type="hidden" name="id" id="id">
                                            <label for="fecha"> Fecha: <span class="danger">*</span> </label>
                                            <input type="date" class="form-control" id="fecha" name="fecha" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 nopadding">
                                        <div class="form-group">
                                            <label for="hora_inicio"> Hora Inicio: <span class="danger">*</span> </label>
                                            <input type="time" class="form-control" id="hora_inicio" name="hora_inicio" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 nopadding">
                                        <div class="form-group">
                                            <label for="hora_fin"> Hora Fin: <span class="danger">*</span> </label>
                                            <input type="time" class="form-control" id="hora_fin" name="hora_fin" value="" required>
   
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>     

                    <!-- ============================================================== -->
                    <!-- Projects of the month -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="d-flex no-block">
                                        <div>
                                            <h4 class="card-title"><span class="lstick"></span>Servicios</h4>
                                        </div>                                        
                                    </div>

                                    <div class="table-responsive m-t-20">
                                        <table class="table vm no-th-brd pro-of-month">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Nombre</th>
                                                    <th>Precio</th>
                                                    <th>Duración</th>
                                                    <th>Estado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($servicio as $serv)
                                                <tr>
                                                    <td style="width:50px;"><span class="round"><img src="../assets/images/users/1.jpg" alt="user" width="50"></span></td>
                                                    <td>
                                                        <h6>{{ $serv->nombre }}</h6><small class="text-muted">{{ $serv->descripcion }}</small>
                                                    </td>
                                                    <td>{{ $serv->valor }}</td>
                                                    <td>{{ $serv->duracion }}</td>
                                                    <td>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="{{ $serv->id }}" name="estado[]" id="estado[{{ $serv->id }}]">
                                                            <label class="form-check-label" for="estado[{{ $serv->id }}]">
                                                                Activo
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>  
                                            @endforeach                                              
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="d-flex no-block">
                                        <div>
                                            <h4 class="card-title"><span class="lstick"></span>Productos</h4>
                                        </div>                                        
                                    </div>

                                    <div class="table-responsive m-t-20">
                                        <table class="table vm no-th-brd pro-of-month">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Nombre</th>
                                                    <th>Precio</th>
                                                    <th>Duración</th>
                                                    <th>Estado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($producto as $pro)
                                                <tr>
                                                    <td style="width:50px;"><span class="round"><img src="../assets/images/users/1.jpg" alt="user" width="50"></span></td>
                                                    <td>
                                                        <h6>{{ $pro->nombre }}</h6><small class="text-muted">{{ $pro->descripcion }}</small>
                                                    </td>
                                                    <td>{{ $pro->valor }}</td>
                                                    <td>{{ $pro->duracion }}</td>
                                                    <td>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="{{ $pro->id }}" name="estado[]" id="estado[{{ $pro->id }}]">
                                                            <label class="form-check-label" for="estado[{{ $pro->id }}]">
                                                                Activo
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>  
                                            @endforeach                                              
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>    
                    </div>      

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_horario" onclick="Horario.formSave()" type="button" class="btn btn-primary" data-dismiss="modal" data-backdrop="false">Guardar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-solicitud">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">solicitud</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_solicitud">
                    
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-sm-12">

                                <div class="row">
                                    <div class="col-sm-4 nopadding">
                                        <div class="form-group">
                                            <input type="hidden" name="id" id="id">
                                            <label for="fecha"> Fecha: <span class="danger">*</span> </label>
                                            <input type="date" class="form-control" id="fecha" name="fecha" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 nopadding">
                                        <div class="form-group">
                                            <label for="hora_inicio"> Hora Inicio: <span class="danger">*</span> </label>
                                            <input type="time" class="form-control" id="hora_inicio" name="hora_inicio" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 nopadding">
                                        <div class="form-group">
                                            <label for="hora_fin"> Hora Fin: <span class="danger">*</span> </label>
                                            <input type="time" class="form-control" id="hora_fin" name="hora_fin" value="" required>
   
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>     

                    <!-- ============================================================== -->
                    <!-- Projects of the month -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">

                                    <div class="d-flex no-block">
                                        <div>
                                            <h4 class="card-title"><span class="lstick"></span>Servicios</h4>
                                        </div>                                        
                                    </div>

                                    <div class="table-responsive m-t-20">
                                        <table class="table vm no-th-brd pro-of-month">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Nombre</th>
                                                    <th>Precio</th>
                                                    <th>selección</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($servicio as $serv)
                                                <tr>
                                                    <td style="width:50px;"><span class="round"><img src="../assets/images/users/1.jpg" alt="user" width="50"></span></td>
                                                    <td>
                                                        <h6>{{ $serv->nombre }}</h6><small class="text-muted">{{ $serv->descripcion }}</small>
                                                    </td>
                                                    <td>{{ $serv->valor }}</td>
                                                    <td>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="{{ $serv->id }}" name="PSdata[]" id="PSdata[{{ $serv->id }}]" onclick="Horario.PSdataCheck({{ $serv }})">
                                                            <label class="form-check-label" for="PSdata[{{ $serv->id }}]">
                                                                .
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>  
                                            @endforeach                                              
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="d-flex no-block">
                                        <div>
                                            <h4 class="card-title"><span class="lstick"></span>Productos</h4>
                                        </div>                                        
                                    </div>

                                    <div class="table-responsive m-t-20">
                                        <table class="table vm no-th-brd pro-of-month">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Nombre</th>
                                                    <th>Precio</th>
                                                    <th>selección</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($producto as $pro)
                                                <tr>
                                                    <td style="width:50px;"><span class="round"><img src="../assets/images/users/1.jpg" alt="user" width="50"></span></td>
                                                    <td>
                                                        <h6>{{ $pro->nombre }}</h6><small class="text-muted">{{ $pro->descripcion }}</small>
                                                    </td>
                                                    <td>{{ $pro->valor }}</td>
                                                    <td>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="{{ $pro->id }}" name="PSdata[]" id="PSdata[{{ $pro->id }}]">
                                                            <label class="form-check-label" for="PSdata[{{ $pro->id }}]">
                                                                .
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>  
                                            @endforeach                                              
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                        
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Total Solicitud</h4>
                                    <h6 class="card-subtitle"><code>Revise su solicitud antes de Guardar</code></h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Cant.</th>
                                                    <th>Nombre</th>
                                                    <th>duración</th>
                                                    <th class="text-nowrap">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div> 

                         

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_solicitud" onclick="" type="button" class="btn btn-primary" data-dismiss="modal" data-backdrop="false">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- Calendar JavaScript -->
<script src="{{ asset('assets/plugins/calendar/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/calendar/dist/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/plugins/switchery/dist/switchery.min.js') }}"></script>
<!---<script src="{{ asset('assets/plugins/dff/dff.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/calendar/dist/lang/es.js') }}"></script>
<script src="{{ asset('assets/plugins/calendar/dist/cal-init.js') }}"></script>--->

<script type="text/javascript">
	
    $(document).ready(function() {

        Horario = {
            url_horarios:"{{ route('agenda.save') }}",
            //menu:$('a.menu_item.active'),
            token:"{{csrf_token()}}",
            init:function(){
                $('form').on('submit', function (ev) {
                ev.preventDefault();
                });

                

                $('#calendario').fullCalendar({
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                    dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    buttonText: {
                        today:    'Hoy',
                        month:    'Mes',
                        week:     'Semana',
                        day:      'Día',
                        list:     'Lista'
                    },
                    header: {
                        left: 'prev, next NuevoHorario',
                        center: 'title',
                        right: 'month, agendaWeek, agendaDay'
                    },
                    views: {
                        week: {
                            columnFormat:'ddd D',
                        },
                    },
                             
                    contentHeight: 'auto',
                    defaultView: 'agendaWeek',
                    allDaySlot: false,
                    editable: true,
                    //eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    selectHelper: true,
                    minTime: "08:00:00",
                    maxTime: "24:00:00",
                    slotLabelFormat: "HH:mm",
                    slotDuration: "00:15:00",
                    slotLabelInterval: "00:15:00",
                    slotWidth: 15,
                    selectConstraint: 'no',
                    eventConstraint: 'no', 
                    events: "{{url('agenda/All')}}/",
                    eventClick:  function(event, jsEvent, view) {  // when some one click on any event
                        endtime = $.fullCalendar.moment(event.end).format('h:mm');
                        starttime = $.fullCalendar.moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
                        //var mywhen = starttime + ' - ' + endtime;
                        //alert(mywhen);

                        $('#modal-evento').modal('show');  
                    },
                    select: function (start, end, allDay) {
                        var fecha = $.fullCalendar.formatDate(start, "Y-MM-DD");
                        var start = $.fullCalendar.formatDate(start, "HH:mm");
                        var end = $.fullCalendar.formatDate(end, "HH:mm");

                        $('#form_solicitud input[name="id"]').val('');
                        $('#form_solicitud input[name="fecha"]').val(fecha);
                        $('#form_solicitud input[name="hora_inicio"]').val(start);
                        $('#form_solicitud input[name="hora_fin"]').val(end);
                        $('#form_solicitud input[type="checkbox"]').prop("checked",false);
                        $('#modal-solicitud').modal('show');  
                    }
                    
                    
                });
                var start = $('#calendario').fullCalendar('getView').start;
                var end = $('#calendario').fullCalendar('getView').end;
                $("#tabla_horarios").load("{{url('tabla/horarios')}}/"+start.format('YYYY-MM-DD')+"/"+end.format('YYYY-MM-DD'));

                
            },
            modalHorario:function(id){

                $('#form_horario input[name="id"]').val('');
                $('#form_horario input[name="fecha"]').val('');
                $('#form_horario input[name="hora_inicio"]').val('');
                $('#form_horario input[name="hora_fin"]').val('');
                $('#form_horario input[type="checkbox"]').prop("checked",false);
                //$('#estado').prop('checked',false);

                $.ajax({
                        url:"{{url('agenda/horario')}}/",
                        type:'POST',
                        data:{
                            id:id,
                            _token:"{{ csrf_token() }}"
                        }
                }).done(function(data){

                    if(data){
                        
                        $('#form_horario input[name="id"]').val(data.id);
                        $('#form_horario input[name="fecha"]').val(data.fecha);
                        $('#form_horario input[name="hora_inicio"]').val(data.hora_inicio);
                        $('#form_horario input[name="hora_fin"]').val(data.hora_fin);
                        $.each(data.agenda_producto_servicios,function(i,v){
                            $('#form_horario input[value="'+v+'"]').prop("checked",true);
                        });
                    }
                        
                });

                $('#modal-horario').modal('show');

            },
            modalNuevo:function(){
                $('#form_horario input[name="id"]').val('');
                $('#form_horario input[name="fecha"]').val('');
                $('#form_horario input[name="hora_inicio"]').val('');
                $('#form_horario input[name="hora_fin"]').val('');
                $('#form_horario input[type="checkbox"]').prop("checked",false);
                $('#modal-horario').modal('show');

            },
            PSdataCheck:function(ps){
                alert(ps.id);
            },
            formSave:function(){
                
                var formElement = document.getElementById("form_horario");
                
                var ajax = App.ajaxData(this.url_horarios,formElement);

                if (ajax){
                    ajax.done(function(data){
                        $('#modal-horario').modal('hide');                   
                        $('a.menu_item.active').click();
                        
                    });
                }
            }
        }
        
        Horario.init();

        

    });

    var room = 1;

        function horario_fields() {

            room++;
            var objTo = document.getElementById('horarios_turno')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML = '<div class="row"><div class="col-sm-4 nopadding"><div class="form-group"><input type="hidden" name="id[]" id="id"><input type="date" class="form-control" id="fecha" name="fecha[]" value=""></div></div><div class="col-sm-4 nopadding"><div class="form-group"><input type="time" class="form-control" id="hora_inicio" name="hora_inicio[]" value=""></div></div><div class="col-sm-4 nopadding"><div class="form-group"><div class="input-group"><input type="time" class="form-control" id="hora_fin" name="hora_fin[]" value=""><div class="input-group-append"><button class="btn btn-danger" type="button" onclick="remove_fields(' + room + ');"> <i class="fa fa-minus"></i></button></div></div></div></div><div class="clear"></div></row>';

            objTo.appendChild(divtest)
        }

        function horario_fields_old(id, fecha, inicio, fin) {

            room++;
            var objTo = document.getElementById('horarios_turno')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML = '<div class="row"><div class="col-sm-4 nopadding"><div class="form-group"><input type="hidden" name="id[]" id="id" value="'+id+'"><input type="date" class="form-control" id="fecha" name="fecha[]" value="'+fecha+'"></div></div><div class="col-sm-4 nopadding"><div class="form-group"><input type="time" class="form-control" id="hora_inicio" name="hora_inicio[]" value="'+inicio+'"></div></div><div class="col-sm-4 nopadding"><div class="form-group"><div class="input-group"><input type="time" class="form-control" id="hora_fin" name="hora_fin[]" value="'+fin+'"><div class="input-group-append"><button class="btn btn-danger" type="button" onclick="remove_fields(' + room + ');"> <i class="fa fa-minus"></i></button></div></div></div></div><div class="clear"></div></row>';

            objTo.appendChild(divtest)
        }

        function remove_fields(rid) {
            $('.removeclass' + rid).remove();
        }

        function remove_fields_old() {
            $('[class*=removeclass]').remove();            
        }

</script>