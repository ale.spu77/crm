<link rel="stylesheet" href="{{asset('assets/plugins/dropify/dist/css/dropify.min.css')}}">
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Lista de Productos y Servicios</h4>
        <h6 class="card-subtitle pull-right" style="float: right;">
            <button onclick="ProducoServicio.nuevo()" type="button" class="btn btn-info">Nuevo Producto o Servicio</button>
        </h6>
        <div class="table-responsive m-t-40">
            <table id="empresas" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Imagen</th>
                        <th>Tipo</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Valor</th>
                        <th>Duración</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
               
                <tbody>
                	@foreach($producto_servicio as $ps)
                    <?php $row = $ps->tipo ? 'table-success':'table-danger' ?>
                    <tr class="{{$row}}">
                        <td>{{ $ps->id }}</td>
                        <td style="width:50px;"><span class="round"><img src="../assets/images/users/1.jpg" alt="user" width="50"></span></td>
                        @if($ps->tipo=='1')
                            <td>Servicio</td>
                        @else
                            <td>Producto</td>
                        @endif
                        <td>{{ $ps->nombre }}</td>
                        <td>{{ $ps->descripcion }}</td>
                        <td>{{ $ps->valor }}</td>
                        <td>{{ $ps->duracion }}</td>
                        <td>
                            <button class="btn btn-primary" onclick="ProducoServicio.modalEmpresa({{$ps->id}})">Editar</button>

                            <button 
                                class="btn btn-danger" 
                                onclick="ProducoServicio.deleteEmpresa({{$ps->id}})">Desactivar</button>
                            <button 
                                class="btn btn-success" 
                                onclick="ProducoServicio.activarEmpresa({{$ps->id}})">Activar</button>

                            <button class="btn btn-primary" onclick="ProducoServicio.sucursal({{$ps->id}})">Sucursales</button>
                        </td>
                    </tr>
                	@endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ProductoServicio">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Producto o Servicio</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_ProductoServicio" enctype="multipart/form-data">
                    <input type="hidden" name="id">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <select class="form-control custom-select" name="tipo">
                                    <option value="">Selecciona</option>
                                    <option value="">Servicio</option>
                                    <option value="">Producto</option>
                                </select>
                                <small class="form-control-feedback"> Selecciona Servicio o Producto. </small> 
                            </div>
                            <div class="form-group">
                                <label for="nombre"> Nombre Producto o Servicio: <span class="danger">*</span> </label>
                                <input type="text" class="form-control" name="nombre" > 
                            </div>
                            
                            <div class="form-group">
                                <label for="descripcion"> Descripción: <span class="danger">*</span> </label>
                                <textarea class="form-control"  name="descripcion" rows="5"></textarea> 
                            </div>

                            <div class="form-group">
                                <label for="valor"> Valor: <span class="danger">*</span> </label>
                                <input type="number" class="form-control" name="valor" > 
                            </div>

                            <div class="form-group">
                                <label for="duracion"> Duración: <span class="danger">*</span> </label>
                                <input type="time" class="form-control" name="duracion" > 
                            </div>

                            <div class="form-group">
                                <label for="imagen"> Imagen Producto o Servicio: <span class="danger">*</span> </label>                                
                                <input type="file" accept="image/*" id="imagen" class="dropify-fr" name="imagen" />
                            </div>                                                        
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_ProductoServicio" onclick="ProducoServicio.formSave()" type="button" class="btn btn-primary" data-dismiss="modal" data-backdrop="false">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<!-- jQuery file upload -->
<script src="{{asset('assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript">
	
$(document).ready(function() {

    ProducoServicio = {
        url_ProductoServicio:"{{ route('ProductoServicio.save') }}",
        //menu:$('a.menu_item.active'),
        token:"{{csrf_token()}}",
        init:function(){
            $('form').on('submit', function (ev) {
              ev.preventDefault();
            });
        },
        activarEmpresa:function(id){
            $.ajax({
                url:"{{url('empresa/activar')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
                $('a.menu_item.active').click();
            });

        },
        deleteEmpresa:function(id){

            $.ajax({
                url:"{{url('empresa/delete')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
               $('a.menu_item.active').click();
            });
        },
        nuevo:function(e){

            $('#form_ProductoServicio input[name="id"]').val('');
            $('#form_ProductoServicio input[name="nombre"]').val('');
            $('#rubro > option').prop('selected',false);
            $('#modal-ProductoServicio').modal('show');

        },
        sucursal:function(id){

                $('#app_container').html('');
                $('#app_container').html($('.preloader').clone().show());

                //alert(app.menu_url);

                item = 'sucursal.lista.empresa';
                $.ajax({
                    url:app.menu_url,
                    type:'POST',
                    data:{
                        item:item,
                        id:id,
                        _token:"{{ csrf_token() }}"
                    }
                }).done(function(data){
                    $('.preloader').hide();
                    $('#app_container').html(data);
                })

        },
        modalEmpresa:function(id){
            $('#form_empresa input[name="id"]').val('');
            $('#form_empresa input[name="nombre"]').val('');
            $('#rubro > option').prop('selected',false);

            $.ajax({
                url:"{{url('empresa')}}/"+id,
                type:"POST",
                data:{
                    _token:this.token
                }
            }).done(function(data){

                if(data){
                    $('#form_empresa input[name="id"]').val(data.id);
                    $('#form_empresa input[name="nombre"]').val(data.nombre);
                    $.each(data.rubro_empresas,function(i,v){
                        //alert(v);
                        $('#rubro > option[value="'+v+'"]').prop('selected',true);
                    });

                }

            });

            $('#modal-empresa').modal('show');
        },
        formSave:function(){
            
            var formElement = document.getElementById("form_ProductoServicio");
            
            var ajax = App.ajaxData(this.url_ProductoServicio,formElement);

            if (ajax){
                ajax.done(function(data){
                    $('#modal-ProductoServicio').modal('hide');                   
                    $('a.menu_item.active').click();
                    
                });
            }
        }
    }
    
    ProducoServicio.init();


  // Basic
$('.dropify').dropify();

// Translated
$('.dropify-fr').dropify({
    messages: {
        default: 'Arrastra el archivo o presiona para examinar',
        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
        remove: 'Remover',
        error: 'hubo un error con la carga de tu archivo'
    }
});

// Used events
var drEvent = $('#input-file-events').dropify();

drEvent.on('dropify.beforeClear', function(event, element) {
    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
});

drEvent.on('dropify.afterClear', function(event, element) {
    alert('File deleted');
});

drEvent.on('dropify.errors', function(event, element) {
    console.log('Has Errors');
});

var drDestroy = $('#input-file-to-destroy').dropify();
drDestroy = drDestroy.data('dropify')
$('#toggleDropify').on('click', function(e) {
    e.preventDefault();
    if (drDestroy.isDropified()) {
        drDestroy.destroy();
    } else {
        drDestroy.init();
    }
})
/*
	$('#usuarios').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    */
});

</script>
