
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Lista de Empresas</h4>
        <h6 class="card-subtitle pull-right" style="float: right;">
            <button onclick="Empresa.nuevo()" type="button" class="btn btn-info">Nueva Empresa</button>
        </h6>
        <div class="table-responsive m-t-40">
            <table id="empresas" class="display responsive nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Sucursales</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
               
                <tbody>
                	@foreach($empresas as $empresa)
                    <?php $row = $empresa->activado ? 'table-success':'table-danger' ?>
                    <tr class="{{$row}}">
                        <td>{{ $empresa->id }}</td>
                        <td>{{ $empresa->nombre }}</td>
                        <td>{{ $empresa->created_at }}</td>
                        <td>{{ $empresa->sucursal->implode('nombre', ' - ') }}</td>
                        <td>
                            <button class="btn btn-primary" onclick="Empresa.modalEmpresa({{$empresa->id}})">Editar</button>

                            @if($empresa->activado)
                            <button 
                                class="btn btn-danger" 
                                onclick="Empresa.deleteEmpresa({{$empresa->id}})">Desactivar</button>
                            @else
                            <button 
                                class="btn btn-success" 
                                onclick="Empresa.activarEmpresa({{$empresa->id}})">Activar</button>
                            @endif

                            <button class="btn btn-primary" onclick="Empresa.sucursal({{$empresa->id}})">Sucursales</button>
                        </td>
                    </tr>
                	@endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-empresa">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Empresa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_empresa">
                    <input type="hidden" name="id">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre"> Nombre Empresa: <span class="danger">*</span> </label>
                                <input type="text" class="form-control required" name="nombre"> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Selecciona Rubros:*</label>
                                <select class="form-control" multiple="" id="rubro" name="rubro[]">
                                @foreach($rubros as $ru)
                                    <option value="{{ $ru->id }}">{{ $ru->nombre }}</option>
                                @endforeach                                                 
                                </select>
                            </div>
                        </div>

                    </div>                   

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_empresa" onclick="Empresa.formSave()" type="button" class="btn btn-primary" data-dismiss="modal" data-backdrop="false">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">
	
$(document).ready(function() {

    Empresa = {
        url_empresa:"{{ route('empresa.save') }}",
        //menu:$('a.menu_item.active'),
        token:"{{csrf_token()}}",
        init:function(){
            $('form').on('submit', function (ev) {
              ev.preventDefault();
            });
        },
        activarEmpresa:function(id){
            $.ajax({
                url:"{{url('empresa/activar')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
                $('a.menu_item.active').click();
            });

        },
        deleteEmpresa:function(id){

            $.ajax({
                url:"{{url('empresa/delete')}}/"+id,
                type:"POST",
                data:{
                    id:id,
                    _token:this.token
                }
            }).done(function(){
               $('a.menu_item.active').click();
            });
        },
        nuevo:function(e){

            $('#form_empresa input[name="id"]').val('');
            $('#form_empresa input[name="nombre"]').val('');
            $('#rubro > option').prop('selected',false);
            $('#modal-empresa').modal('show');

        },
        sucursal:function(id){

                $('#app_container').html('');
                $('#app_container').html($('.preloader').clone().show());

                //alert(app.menu_url);

                item = 'sucursal.lista.empresa';
                $.ajax({
                    url:app.menu_url,
                    type:'POST',
                    data:{
                        item:item,
                        id:id,
                        _token:"{{ csrf_token() }}"
                    }
                }).done(function(data){
                    $('.preloader').hide();
                    $('#app_container').html(data);
                })

        },
        modalEmpresa:function(id){
            $('#form_empresa input[name="id"]').val('');
            $('#form_empresa input[name="nombre"]').val('');
            $('#rubro > option').prop('selected',false);

            $.ajax({
                url:"{{url('empresa')}}/"+id,
                type:"POST",
                data:{
                    _token:this.token
                }
            }).done(function(data){

                if(data){
                    $('#form_empresa input[name="id"]').val(data.id);
                    $('#form_empresa input[name="nombre"]').val(data.nombre);
                    $.each(data.rubro_empresas,function(i,v){
                        //alert(v);
                        $('#rubro > option[value="'+v+'"]').prop('selected',true);
                    });

                }

            });

            $('#modal-empresa').modal('show');
        },
        formSave:function(){
            
            var formElement = document.getElementById("form_empresa");
            
            var ajax = App.ajaxData(this.url_empresa,formElement);

            if (ajax){
                ajax.done(function(data){
                    $('#modal-empresa').modal('hide');                   
                    $('a.menu_item.active').click();
                    
                });
            }
        }
    }
    
    Empresa.init();
/*
	$('#usuarios').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    */
});

</script>
