<div class="card">
    <div class="card-body">
        <h4 class="card-title">Lista de Locales</h4>
        <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
        <div class="table-responsive m-t-40">
            <table id="locales" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Fecha creación</th>
                    <th>Opciones</th>
                </tr>
                </tfoot>
                <tbody>
                	@foreach($locales as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td>
                            <button data-id="{{ $item->id }}" class="btn btn-success" onclick="Local.modalLocal(this)">Editar</button>
                        </td>
                    </tr>
                	@endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-id">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Local</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_local">
                    <input type="hidden" name="id">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Nlocal"> Nombre Local: <span class="danger">*</span> </label>
                            <input type="text" class="form-control required" name="Nlocal"> 
                        </div>
                    </div>
                        <div class="col-md-6">
                            
                </div>
                </form>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="guardar_local" onclick="Local.formSave()" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
    Local = {
        url_local:"{{ route('local.saveLocal') }}",
        menu:$('a.menu_item.active'),
        token:"{{csrf_token()}}",
        init:function(){
            $('form').on('submit', function (ev) {
              ev.preventDefault();
            });
        },
        modalLocal:function(e){
            console.log(e);
            var id = $(e).data('id');
            $('#form_local input[name="Nlocal"]').val('');

            $.ajax({
                url:"{{url('local')}}/"+id,
                type:"POST",
                data:{
                    _token:this.token
                }
            }).done(function(data){

                
                if(data){
                    //alert("har");
                    $('#form_local input[name="id"]').val(data.id_local);
                    $('#form_local input[name="Nlocal"]').val(data.nombrelocal);

                }

            });

            

            $('#modal-id').modal('show');
        },
        formSave:function(){
            var formElement = document.getElementById("form_local");  
            alert(formElement);          
            var ajax = App.ajaxData(this.url_local,formElement);
            if (ajax){
                ajax.done(function(data){
                    $('#modal-id').modal('hide');
                    //$('a.menu_item.active').click();
                });
            }


        }
    }
    
    Local.init();

	
	

});


/*
$('#locales').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

*/



</script>
