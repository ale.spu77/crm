
 <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Validation wizard -->
                <div class="row" id="tab-wizard">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <h4 class="card-title">Para Crear Un Nuevo Local</h4>
                                <h6 class="card-subtitle">Sigue los pasos</h6>
                                <form action="{{ route('local.CrearLocal') }}" method="post" class="tab-wizard wizard-circle">
                                    <!-- Step 1 -->
                                    @csrf
                                    <h6>Local-Sucursal</h6>
                                    <section>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Nlocal"> Nombre Local: <span class="danger">*</span> </label>
                                                    <input type="text" class="form-control required" id="Nlocal" name="Nlocal"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Nsucursal"> Nombre Sucursal : <span class="danger">*</span> </label>
                                                    <input type="text" class="form-control required" id="Nsucursal" name="Nsucursal"> </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                            <div class="form-group">
                                            <label>Selecciona Rubros:*</label>
                                                <select class="form-control" multiple="" id="rubro[]" name="rubro[]">
                                                @foreach($rubros as $Item)
                                                    <option value="{{ $Item->id }}">{{ $Item->nombre }}</option>
                                                @endforeach                                                 
                                                </select>
                                        </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="dsucursal"> Dirección Sucursal :</label>
                                                    <input type="tel" class="form-control" id="dsucursal" name="dsucursal"> </div>
                                            </div>
                                        </div>
                                    </section>
                                    <!-- Step 2 -->
                                    <h6>Empresa</h6>
                                    <section>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="rsocial">Razón Social :</label>
                                                    <input type="text" class="form-control" id="rsocial" name="rsocial">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="rutEmpresa">Rut :</label>
                                                    <input type="text" class="form-control" id="rutEmpresa" name="rutEmpresa"> </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="dirComercial">Dirección Comercial :</label>
                                                    <input type="text" class="form-control" id="dirComercial" name="dirComercial">
                                                </div>

                                            </div>
                                        </div>
                                        
                                    </section>
                                    <!-- Step 3 -->
                                    <h6>Contacto</h6>
                                    <section>

                                    <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="telefono">Teléfono :</label>
                                                    <input type="tel" class="form-control" id="telefono" name="telefono">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">email :</label>
                                                    <input type="email" class="form-control required" id="email" name="email"> </div>
                                            </div>
                                        </div>
                                        
                                    </section>
                                    <!-- Step 4 -->
                                    <h6>Final</h6>
                                    <section>
                                    <div class="row">
                                            <div class="col-md-8">
                                                <div class="alert alert-info">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                                    <h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Información</h3> Al presionar "Guardar" se creara un nuevo local.
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
<script type="text/javascript">
	
    $(".tab-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "fade"
    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        cancel: "Cancelar",
        current: "current step:",
        pagination: "Paginas",
        finish: "Guardar",
        next: "Siguente",
        previous: "Anterior",
        loading: "Esperando ..."
    }
    , onFinished: function (event, currentIndex) {
       var form = $(this);

        form.submit();
    }
    });
    
</script>
